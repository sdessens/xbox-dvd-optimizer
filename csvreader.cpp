#include "csvreader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

#include "cstdlib"
#include "math.h"

#include "resource.h"
#include "batch.h"

CsvReader::CsvReader(const char* filename)
{
    path = filename;
}

void CsvReader::read()
{
    std::ifstream  data(path);
    if (!data.is_open())
    {
        cout << "failed to open " << path << endl;
    }


    Resource *r;
    Batch *b;
    int n;
    int offset = 0;
    double doffset = 0;

    std::string line;
    while(std::getline(data,line))
    {
        std::stringstream  lineStream(line);
        std::string        cell;

        r = NULL;

        while(std::getline(lineStream,cell,','))
        {
            if (cell == "\r")   // fix for linux
                continue;

            n = atoi(cell.c_str());

            if (r == NULL)
            {
                r = new Resource;
                r->size = n;
                r->offset = offset;
                offset += r->size;
                doffset += r->size;
            } else {
                b = Batch::get(n);
                r->batches.push_back(b);
                b->addResource(r);
            }
        }
    }

    cout << "read:" <<
            "\t" << Resource::resourcePool.size() << " resources" << endl <<
            "\t" << Batch::count() << " batches" << endl <<
            "\t" << offset << " offset (" << log(offset) << " bit)" << endl <<
            "\t" << doffset << " offset-d (" << log(doffset) << " bit)" << endl;
//    cin.get();

}
