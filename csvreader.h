#ifndef CSVREADER_H
#define CSVREADER_H

class CsvReader
{
public:
    CsvReader(const char *filename);
    void read();
private:
    const char* path;
};

#endif // CSVREADER_H
