#-------------------------------------------------
#
# Project created by QtCreator 2012-11-18T14:53:41
#
#-------------------------------------------------

TARGET = XBOX-dvd
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    csvreader.cpp \
    resource.cpp \
    batch.cpp \
    fitness.cpp

HEADERS += \
    csvreader.h \
    resource.h \
    batch.h \
    fitness.h
