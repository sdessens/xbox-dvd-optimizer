#ifndef FITNESS_H
#define FITNESS_H

#include <vector>
#include <list>
using namespace std;

#include "resource.h"

class Fitness
{

public:
    static int oisynFitness(vector<Resource *>& resources);
    static int oisynFitness(list<Resource *>& resources);

    static vector<Resource*> optimize(vector<Resource*> resources);
    static void continuousOptimization(vector<Resource*>& resources);

    Fitness();
};

#endif // FITNESS_H
