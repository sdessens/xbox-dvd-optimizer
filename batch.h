#ifndef BATCH_H
#define BATCH_H

#include <vector>
#include <map>

#define RESOURCE_MAX 500

class Resource;

class Batch
{
public:
    static Batch* get(int n);
    static int count();
    Resource* resourcesArray[RESOURCE_MAX];
    std::vector<Resource* > resources;

    void addResource(Resource* r);

    int id;


    void sortResources();


    static int maxId;
    int resourceCount;

private:

    static std::map<int, Batch* > batches;
    Batch(int n);

};

#endif // BATCH_H
