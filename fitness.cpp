#include "fitness.h"

#include <iostream>
#include <algorithm>
#include <set>
using namespace std;

#include "batch.h"
#include <cassert>

#define OUT_OF_BOUNDS (1352335360 + 1) // = the maximum offset in the test data + 1.

inline int oisynCost(int from, int to) {
    int d = to - from;
    return d > (32*1024) ? 75 + d / (34761904) : 0;
}

int Fitness::oisynFitness(vector<Resource*>& resources)
{
    int totalcost = 0;

    int id;
    int offset = 0;
    Batch* batch;

    int lastRead[Batch::maxId+1];
    for(int i = 0; i <= Batch::maxId; i++)
        lastRead[i] = 0;

    for(vector<Resource*>::iterator r = resources.begin(); r != resources.end(); r++)
    {
        Resource* res = (*r);
        for(vector<Batch*>::iterator b = res->batches.begin(); b != res->batches.end(); b++)
        {
            batch = (*b);
            id = batch->id;
            if (lastRead[id])
                totalcost += oisynCost(lastRead[id], offset);
            lastRead[id] = offset + res->size;
        }
        offset += res->size;
    }

    return totalcost;
}

int Fitness::oisynFitness(list<Resource*>& resources)
{
    int totalcost = 0;

    int id;
    int offset = 0;
    Batch* batch;

    int lastRead[Batch::maxId+1];
    for(int i = 0; i <= Batch::maxId; i++)
        lastRead[i] = 0;

    for(list<Resource*>::iterator r = resources.begin(); r != resources.end(); r++)
    {
        Resource* res = (*r);
        for(vector<Batch*>::iterator b = res->batches.begin(); b != res->batches.end(); b++)
        {
            batch = (*b);
            id = batch->id;
            if (lastRead[id])
                totalcost += oisynCost(lastRead[id], offset);
            lastRead[id] = offset + res->size;
        }
        offset += res->size;
    }

    return totalcost;
}



int resourceFitness(const Resource* resource)
{
    Batch* b;
    Resource* r;
    int lastSeen;
    int totalCost = 0;

    for(vector<Batch*>::const_iterator i = resource->batches.begin(); i != resource->batches.end(); i++)
    {
        b = (*i);

        b->sortResources();

        lastSeen = 0;

        int count;
        count = b->resourceCount;
        Resource* r;

        r = b->resourcesArray[0];
        lastSeen = r->offset + r->size;
        for(int i = 1; i < count; i++)
        {
            r = b->resourcesArray[i];
            totalCost += oisynCost(lastSeen, r->offset);
            lastSeen = r->offset + r->size;
        }

//        lastSeen = 0;

//        for(vector<Resource*>::const_iterator j = b->resources.begin(); j < b->resources.end(); j++)
//        {
//            r = (*j);
//            if (r->offset == OUT_OF_BOUNDS)
//                break;

//            if (lastSeen)
//            {
//                totalCost += oisynCost(lastSeen, r->offset);
//            }
//            lastSeen = r->offset + r->size;
//        }

    }
//    cout << totalCost << endl << endl;
    return totalCost;
}

void mostEfficientPlacement(list<Resource* >& resources, Resource* r)
{
    resources.push_front(r);
    if (resources.size() == 1)
        return;

    // zet de offset van elke resource goed. Kan nog verder geoptimaliseerd worden.
    int offset = 0;
    for(list<Resource*>::iterator i = resources.begin();
        i != resources.end();
        i++)
    {
        (*i)->offset = offset;
        offset += (*i)->size;
    }

    int index = 0;
    int bestIndex = 0;
    int bestFitness = 999999;
    list<Resource*>::iterator bestIterator = resources.begin();

    int insertFitness = resourceFitness(r); // fitness of 'r'
    int fitness = insertFitness;
    int lastFitness;

    int bestOisynFitness = Fitness::oisynFitness(resources);
    int newBestOisynFitness;

    list<Resource*>::iterator prevIterator;
    Resource* tmp;

    for(list<Resource*>::iterator resourceIterator = resources.begin();
        resourceIterator != resources.end();
        resourceIterator++)
    {


        // swap elements
        // als we een element swappen:
        // bereken fitness van beide resources
        // trek dat van het totaal af
        // swap
        // herbereken fitness van beide resources
        // tel dat bij het totaal op.
        // maar let op: zorg dat de offset van de resources correct blijft
        if (index != 0) {

            // setup pointer naar prev. object.
            prevIterator = resourceIterator;
            prevIterator--;

            // subtract fitness
            lastFitness = resourceFitness(*resourceIterator);
            fitness -= insertFitness;
            fitness -= lastFitness;

            // swap resources
            tmp = (*resourceIterator);
            (*resourceIterator) = (*prevIterator);
            (*prevIterator) = tmp;

            // fix offsets
            (*prevIterator)->offset = (*resourceIterator)->offset;
            (*resourceIterator)->offset = (*prevIterator)->offset + (*prevIterator)->size;

            // add fitness
            insertFitness = resourceFitness(r);
            lastFitness = resourceFitness(*prevIterator);
            fitness += lastFitness;
            fitness += insertFitness;

        }

        //evaluate fitness
        if (index == 0)
        {
            bestIterator = resourceIterator;
            bestFitness = fitness;
            bestIndex = index;

//            bestOisynFitness = Fitness::oisynFitness(resources);
//            newBestOisynFitness = bestOisynFitness;
        }
        else if (fitness <= bestFitness)
        {
//            newBestOisynFitness = Fitness::oisynFitness(resources);
//            if (!(newBestOisynFitness <= bestOisynFitness))
//            {
//                cout << "warning: suboptimal placement" << endl;
//                cout << "new oisyn: " << newBestOisynFitness << endl;
//                cout << "old oisyn: " << bestOisynFitness;
//                cout << endl;
//            }
//            newBestOisynFitness = bestOisynFitness;

            bestIterator = resourceIterator;
            bestFitness = fitness;
            bestIndex = index;
        }
        index++;
    }

    list<Resource*>::iterator last = resources.end();
    last--;

//    cout << "bestIndex = " << bestIndex << endl;

    // if the best position is the last position, do nothing
    if (last != bestIterator)
    {
        resources.erase(last);
        resources.insert(bestIterator, r);
    }
}


vector<Resource*> Fitness::optimize(vector<Resource *> resources)
{
    for(vector<Resource* >::iterator i = resources.begin(); i != resources.end(); i++)
        (*i)->offset = OUT_OF_BOUNDS;

    list<Resource *> orderedResources;
    vector<Resource* >::iterator del;
    Resource* r;

    for(vector<Resource* >::iterator i = resources.begin(); i != resources.end(); i++)
    {
        r = (*i);   // de resource die we gaan inserten
        mostEfficientPlacement(orderedResources, r);
        cout << "size: " << orderedResources.size() << "\tfitness: " << Fitness::oisynFitness(orderedResources) << endl;

//        if (orderedResources.size() == 1000)
//            break;
    }

    return vector<Resource*>(orderedResources.begin(), orderedResources.end());
}

void Fitness::continuousOptimization(vector<Resource *> &resources)
{
    std::list<Resource *> resourceList(resources.begin(), resources.end());
    Resource* r;
    int pos;
    int iterations = 0;
    int fitness, lastFitness = 99999999;
    while(1) {
        pos = rand() % resources.size();

//        cout << "randomly picked element " << pos << endl;

        r = NULL;
        for(list<Resource* >::iterator i = resourceList.begin(); i != resourceList.end(); i++)
        {
            if (pos-- == 0)
            {
                r = (*i);
                resourceList.erase(i);
                break;
            }
        }
        assert(r);

        mostEfficientPlacement(resourceList, r);

        iterations++;

        fitness = Fitness::oisynFitness(resourceList);
        if (0 == (iterations % 10))
        {
            std::cout << "iteration: " << iterations << "\t" << fitness << endl;
        }

        assert(fitness <= lastFitness);
        lastFitness = fitness;
    }
}

Fitness::Fitness()
{
}
