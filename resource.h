#ifndef RESOURCE_H
#define RESOURCE_H

#include <vector>

class Batch;

class Resource
{
public:
    Resource();
    int size;
    int offset;
    std::vector<Batch* > batches;

    static std::vector<Resource* > resourcePool;
};



#endif // RESOURCE_H
