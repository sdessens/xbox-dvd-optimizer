#include "csvreader.h"

#include <iostream>
using namespace std;

#include "algorithm"
#include <cstdlib>
#include <windows.h>

#include "fitness.h"

int sizeMul = 9472;
bool resourceSort(Resource* a, Resource* b)
{
    return (a->batches.size() * sizeMul + a->size)
            >
            (b->batches.size() * sizeMul + b->size);
}


int main(int argc, char *argv[])
{
    if (argc < 2)
        exit(0);

    CsvReader reader(argv[1]);
    reader.read();

    sort(Resource::resourcePool.begin(), Resource::resourcePool.end(), resourceSort);


    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;

    // get ticks per second
    QueryPerformanceFrequency(&frequency);

    // start timer
    QueryPerformanceCounter(&t1);

    //    int count = 10;
    //    int fitness;
    //    for(int i = 0; i < count; i++)
    //        fitness = Fitness::oisynFitness(Resource::resourcePool);
    //        Fitness::optimize(Resource::resourcePool);
    //    cout << "fitness: " << fitness << std::endl;

    vector<Resource*> OptimalResources = Fitness::optimize(Resource::resourcePool);

    // stop timer
    QueryPerformanceCounter(&t2);

    // compute and print the elapsed time in millisec
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << elapsedTime / 1000 << " s.\n";
    //    cout << 1000 / (elapsedTime / (float)count) << " checks / sec\n";
    cout << endl;
    cout << endl;


    Fitness::continuousOptimization(OptimalResources);


}
