#include "batch.h"

#include "resource.h"

#include "algorithm"
#include "math.h"

std::map<int, Batch* > Batch::batches;
int Batch::maxId = 0;

Batch::Batch(int n)
    :   id(n)
{
    for(int i = 0; i < RESOURCE_MAX; i++)
        resourcesArray[i] = 0;

    resourceCount = 0;

    Batch::batches[id] = this;
    maxId = id > maxId ? id : maxId;
}

Batch* Batch::get(int n)
{
    int c = Batch::batches.count(n);
    if (c == 0)
        return new Batch(n);
    else
        return Batch::batches[n];
}

int Batch::count()
{
    return Batch::batches.size();
}



void Batch::addResource(Resource *r)
{
    resourcesArray[resourceCount++] = r;
    resources.push_back(r);
}

bool resourceComp(Resource* a, Resource* b)
{
    return a->offset < b->offset;
}

bool resourceCompInline(const Resource* a, const Resource* b)
{
    return a->offset < b->offset;
}


void Batch::sortResources()
{
//    sort(
//                resources.begin(),
//                resources.end(),
//                resourceComp);

    int i = 1;
    while(i < resourceCount)
    {
        if (resourceCompInline(resourcesArray[i], resourcesArray[i-1]))
        {
            std::swap(resourcesArray[i], resourcesArray[i-1]);
            i = std::max(0, i-2);
        }
            i++;
    }
}

